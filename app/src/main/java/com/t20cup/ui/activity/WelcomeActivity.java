package com.t20cup.ui.activity;

import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.view.WindowManager;
import android.widget.ImageView;
import androidx.appcompat.app.AppCompatActivity;
import com.bumptech.glide.Glide;
import com.t20cup.R;
import com.t20cup.ui.activity.home.MainActivity;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

public class WelcomeActivity extends BaseActivity {
    private Unbinder unbinder;
    MediaPlayer mp;
    @BindView(R.id.cheersImage)
    ImageView cheersImage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_welcome);
        unbinder = ButterKnife.bind(this);
        Glide.with(this).load(R.drawable.cheersgif).into(cheersImage);
        mp = MediaPlayer.create(this, R.raw.ipl);
        mp.start();
    }

    @OnClick(R.id.enter)
    public void moveToMainScreen() {
        Intent i = new Intent(WelcomeActivity.this, MainActivity.class);
        WelcomeActivity.this.startActivity(i);
        WelcomeActivity.this.finish();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mp.stop();
        mp.release();
        unbinder.unbind();
    }
}
