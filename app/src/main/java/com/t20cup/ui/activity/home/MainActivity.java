package com.t20cup.ui.activity.home;

import android.os.Bundle;
import android.view.View;

import com.t20cup.R;
import com.t20cup.model.Player;
import com.t20cup.util.Constants;
import com.t20cup.util.Utils;

import butterknife.OnClick;

import static com.t20cup.util.Constants.BALL_DEFAULT;
import static com.t20cup.util.Constants.WKT_DEFAULT;

public class MainActivity extends MainUIActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Utils.setEditTextFields(targetScore, Constants.TARGET, R.drawable.bg_target);
        Utils.setEditTextFields(scoreLayout, String.valueOf(scoreCount), R.drawable.bg_target);
        Utils.setEditTextFields(wktLayout, WKT_DEFAULT, R.drawable.bg_wkt);
        Utils.setEditTextFields(overLayout, String.valueOf(oversCount), R.drawable.bg_target);
        Utils.setEditTextFields(ballsLayout, BALL_DEFAULT, R.drawable.bg_ball);
        getBatsmanName();
        setNameAndRun();
        strikerRun.setText(batsmanScore1);
        nonStrikerRun.setText(batsmanScore2);
    }

    @OnClick(R.id.bowlButton)
    public void bowl() {
        if (mpSix != null && mpSix.isPlaying())  mpSix.stop();
        if (mpFour != null && mpFour.isPlaying())  mpFour.stop();
        cheersImage.setVisibility(View.GONE);
        if (scoreCount <= Utils.convertStringToInt(Constants.TARGET)) {
            startBatSound();
            int randomVal = Utils.getRandomVal();
            setScore(randomVal);
            Utils.setEditTextFields(ballsLayout, String.valueOf(ballCount), R.drawable.bg_ball);
            flashCard.setText(String.valueOf(randomVal));
            updateScore(randomVal);
            updateOvers();
            ballCount++;
        }
    }

    private void updateOvers() {
        if (ballCount == 6 && oversCount <= 20) {
            ballCount = 0;
            oversCount = oversCount + 1;
            Utils.setEditTextFields(ballsLayout, String.valueOf(ballCount), R.drawable.bg_ball);
            Utils.setEditTextFields(overLayout, String.valueOf(oversCount), R.drawable.bg_target);
        }
    }

    private void updateScore(int randomVal) {
        if (randomVal > 0) {
            scoreCount = scoreCount + randomVal;
            Utils.setEditTextFields(scoreLayout, String.valueOf(scoreCount), R.drawable.bg_target);
            if (scoreCount >= Utils.convertStringToInt(Constants.TARGET)) {
                mpFour.stop();
                mpSix.stop();
                mpAudience.start();
                Utils.setCustomToastMessage(this, "Bangalore won by 3 wickets", R.layout.alert_win);
            }
        }
    }

    private void setScore(int randomVal) {
        flashCard.setVisibility(View.VISIBLE);
        flashCard.setTextColor(getResources().getColor(android.R.color.black));
        if (randomVal == 0) return;
        if (randomVal == 1 || randomVal == 3 || randomVal == 5) {
            strikerAnim = Utils.setRotateIndicator();
            nonStrikerAnim = Utils.setRotateIndicator();
            strikerCard.startAnimation(strikerAnim);
            nonStrikerCard.startAnimation(nonStrikerAnim);
            if (randomVal == 5) {
                startFourSound();
            }
            isRotateStriker = true;
            totalStrikerRun(randomVal);
            rotateStriker();
        } else {
            if (randomVal == 6) startSixSound();
            if (randomVal == 4) startFourSound();
            totalStrikerRun(randomVal);
        }
    }

    private void totalStrikerRun(int randomVal) {
        String strikerVal = striker.getText().toString();
        for (int i = 0; i < batsmanList.size(); i++) {
            Player player = batsmanList.get(i);
            updatePlayer1Run(randomVal, strikerVal, player);
            updatePlayer2Run(randomVal, strikerVal, player);
            batsmanList.set(i, player);
        }
        isRotateStriker = false;
    }

    private void updatePlayer2Run(int randomVal, String strikerVal, Player player) {
        updatePlayerRun(randomVal, strikerVal, player, batsman2);
    }

    private void updatePlayer1Run(int randomVal, String strikerVal, Player player) {
        updatePlayerRun(randomVal, strikerVal, player, batsman1);
    }

    private void updatePlayerRun(int randomVal, String strikerVal, Player player, String batsman1) {
        if (player.getName().equals(batsman1)) {
            if (strikerVal.equals(player.getName())) {
                int playerScore = player.getScore() + randomVal;
                if (isRotateStriker) {
                    strikerRun.setText(nonStrikerRun.getText().toString());
                    nonStrikerRun.setText(String.valueOf(playerScore));
                } else {
                    strikerRun.setText(String.valueOf(playerScore));
                }
                player.setScore(playerScore);
            }
            switch (randomVal) {
                case 2:
                    player.setTwoCount(player.getTwoCount() + 1);
                    break;
                case 3:
                    player.setThreeCount(player.getThreeCount() + 1);
                    break;
                case 4:
                    player.setFourCount(player.getFourCount() + 1);
                    break;
                case 5:
                    player.setFiveCount(player.getFiveCount() + 1);
                    break;
                case 6:
                    player.setSixCount(player.getSixCount() + 1);
                    break;
                default:
                    player.setOneCount(player.getOneCount() + 1);
            }
        }
    }

    private void rotateStriker() {
        String strikerVal = striker.getText().toString();
        if (strikerVal.equals(batsman2)) {
            setNameAndRun();
        } else {
            striker.setText(batsman2);
            nonStriker.setText(batsman1);
        }
    }
}