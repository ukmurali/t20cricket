package com.t20cup.ui.activity.home;

import android.media.MediaPlayer;
import android.os.Bundle;
import android.view.View;
import android.view.animation.Animation;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.google.android.material.card.MaterialCardView;
import com.t20cup.R;
import com.t20cup.model.Player;
import com.t20cup.ui.activity.BaseActivity;
import com.t20cup.util.Constants;
import com.t20cup.util.Utils;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

import static com.t20cup.util.Constants.BALL_DEFAULT_COUNT;
import static com.t20cup.util.Constants.OVER_DEFAULT_COUNT;
import static com.t20cup.util.Constants.PLAYER_1_SCORE;
import static com.t20cup.util.Constants.PLAYER_2_SCORE;
import static com.t20cup.util.Constants.PLAYER_SCORE;
import static com.t20cup.util.Constants.SCORE_DEFAULT_COUNT;

public class MainUIActivity extends BaseActivity {
    public Unbinder unbinder;
    @BindView(R.id.targetScore)
    LinearLayout targetScore;
    @BindView(R.id.striker)
    TextView striker;
    @BindView(R.id.strikerRun)
    TextView strikerRun;
    @BindView(R.id.nonStriker)
    TextView nonStriker;
    @BindView(R.id.nonStrikerRun)
    TextView nonStrikerRun;
    @BindView(R.id.flashCard)
    TextView flashCard;
    @BindView(R.id.scoreLayout)
    LinearLayout scoreLayout;
    @BindView(R.id.wktLayout)
    LinearLayout wktLayout;
    @BindView(R.id.overLayout)
    LinearLayout overLayout;
    @BindView(R.id.ballsLayout)
    LinearLayout ballsLayout;
    @BindView(R.id.cheersImage)
    ImageView cheersImage;
    @BindView(R.id.strikerCard)
    MaterialCardView strikerCard;
    @BindView(R.id.nonStrikerCard)
    MaterialCardView nonStrikerCard;
    MediaPlayer mp;
    MediaPlayer mpAudience;
    MediaPlayer mpSix;
    MediaPlayer mpFour;
    Animation strikerAnim, nonStrikerAnim;
    public List<Player> batsmanList = new ArrayList<>();
    String batsman1 = "", batsman2 = "", batsmanScore1 = PLAYER_1_SCORE,
            batsmanScore2 = PLAYER_2_SCORE;
    int ballCount = BALL_DEFAULT_COUNT, oversCount = OVER_DEFAULT_COUNT,
            scoreCount = SCORE_DEFAULT_COUNT;
    boolean isRotateStriker = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        unbinder = ButterKnife.bind(this);
        createPlayer(Constants.PLAYER_1, Utils.convertStringToInt(PLAYER_1_SCORE), "5%", "30%", "25%", "10%", "15%", "9%", "5%");
        createPlayer(Constants.PLAYER_2, Utils.convertStringToInt(PLAYER_2_SCORE), "10%", "40%", "20%", "5%", "10%", "4%", "10%");
        createPlayer(Constants.PLAYER_3, Utils.convertStringToInt(PLAYER_SCORE), "5%", "30%", "25%", "10%", "15%", "9%", "5%");
        createPlayer(Constants.PLAYER_4, Utils.convertStringToInt(PLAYER_SCORE), "5%", "30%", "25%", "10%", "15%", "9%", "5%");
    }

    @Override
    protected void onResume() {
        super.onResume();
        startAudienceSound();
    }

    private void createPlayer(String name, int score, String dotBall, String one, String two, String three, String four, String six, String out) {
        Player player = new Player();
        player.setName(name);
        player.setScore(score);
        player.setDotBall(dotBall);
        player.setOne(one);
        player.setTwo(two);
        player.setThree(three);
        player.setFour(four);
        player.setFive("1%");
        player.setSix(six);
        player.setOut(out);
        batsmanList.add(player);
    }

    void startAudienceSound() {
        mpAudience = MediaPlayer.create(this, R.raw.audience);
        mpAudience.start();
        mpAudience.setLooping(true);
    }

    void startFourSound() {
        cheersImage.setVisibility(View.VISIBLE);
        Glide.with(this).load(R.drawable.cheersgif).into(cheersImage);
        flashCard.setTextColor(getResources().getColor(R.color.fourColor));
        mpFour = MediaPlayer.create(this, R.raw.four1);
        mpFour.start();
    }


    void startSixSound() {
        cheersImage.setVisibility(View.VISIBLE);
        Glide.with(this).load(R.drawable.cheersgif).into(cheersImage);
        flashCard.setTextColor(getResources().getColor(R.color.sixColor));
        mpSix = MediaPlayer.create(this, R.raw.sixer);
        mpSix.start();
    }

    void setNameAndRun() {
        striker.setText(batsman1);
        nonStriker.setText(batsman2);
    }

    void getBatsmanName() {
        batsman1 = batsmanList.get(0).getName();
        batsman2 = batsmanList.get(1).getName();
    }

    void startBatSound() {
        mp = MediaPlayer.create(this, R.raw.batsound);
        mp.start();
    }

    void stopMedia(MediaPlayer mediaPlayer) {
        if (mediaPlayer != null) {
            mediaPlayer.stop();
            mediaPlayer.release();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        stopMedia(mp);
        stopMedia(mpAudience);
        stopMedia(mpSix);
        stopMedia(mpFour);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        stopMedia(mp);
        if (strikerAnim != null) strikerAnim.cancel();
        if (nonStrikerAnim != null) nonStrikerAnim.cancel();
        stopMedia(mpAudience);
        stopMedia(mpSix);
        stopMedia(mpFour);
        unbinder.unbind();
    }
}