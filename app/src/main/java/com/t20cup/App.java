package com.t20cup;

import android.app.Application;
import android.content.Context;
import androidx.multidex.MultiDex;
import dagger.ObjectGraph;

public class App extends Application {
    private static final String LOG_TAG = App.class.getName();
    private static App instance;
    private static ObjectGraph graph = null;
    public static App getInstance() {
        return instance;
    }
    public static Context getContext() {
        return instance;
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }

    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;
    }

    @Override
    public void onTerminate() {
        super.onTerminate();
    }

    public ObjectGraph getObjectGraph() {
        return graph;
    }

    public static void setObjectGraph(ObjectGraph objectGraph) {
        graph = objectGraph;
    }

}
