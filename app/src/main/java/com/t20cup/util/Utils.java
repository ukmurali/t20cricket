package com.t20cup.util;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.app.Activity;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.Handler;
import android.text.InputType;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TableLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.dialog.MaterialAlertDialogBuilder;
import com.t20cup.App;
import com.t20cup.R;

import java.util.Random;

public class Utils {
    public static void setEditTextFields(LinearLayout linearLayout, String text, int resId) {
        int[] animatingNumbers;
        TextAnimationFields textAnimationFields;
        textAnimationFields = new TextAnimationFields();
        textAnimationFields.setTextSize((int) App.getContext().getResources().getDimension(R.dimen._8ssp));
        String vvv = "100";
        String gapBetweenTwoNumbersDurationString = "100";
        textAnimationFields.setGapBetweenTwoNumbersDuration(Integer.parseInt(gapBetweenTwoNumbersDurationString));
        textAnimationFields.setAnimationDuration(Integer.parseInt(vvv));
        linearLayout.removeAllViews();

        if (text.length() != 0) {
            textAnimationFields.setMaxNumbers(text.length());
            char[] NArray = text.toCharArray();
            animatingNumbers = new int[7];
            for (int i = 0; i < text.length(); i++) {
                animatingNumbers[i] = Integer.parseInt("" + NArray[i]);
            }
        } else {
            animatingNumbers = new int[]{0, 0, 0, 0};
            textAnimationFields.setMaxNumbers(animatingNumbers.length);
        }
        createNumberOfTextFields(textAnimationFields, linearLayout, animatingNumbers, resId);
    }

    private static void createNumberOfTextFields(TextAnimationFields textAnimationFields,
                                                 LinearLayout linearLayout, int[] animatingNumbers, int resId) {
        RelativeLayout childLayout;
        LinearLayout.LayoutParams linearParams;
        TextView rowTextView, rowTextViewOut;
        TextView[] myTextViews, myTextViewsOut;
        myTextViews = new TextView[textAnimationFields.getMaxNumbers()]; // create an empty array;
        myTextViewsOut = new TextView[textAnimationFields.getMaxNumbers()]; // create an empty array;
        for (int i = 0; i < textAnimationFields.getMaxNumbers(); i++) {
            childLayout = new RelativeLayout(App.getContext());
            linearParams = new LinearLayout.LayoutParams((textAnimationFields.getTextSize() * 3),
                    LinearLayout.LayoutParams.WRAP_CONTENT);
            childLayout.setBackground(App.getContext().getResources().getDrawable(resId));
            linearParams.setMargins( (int) App.getContext().getResources().getDimension(R.dimen._1sdp),
                    (int) App.getContext().getResources().getDimension(R.dimen._1sdp),
                    (int) App.getContext().getResources().getDimension(R.dimen._1sdp),
                    (int) App.getContext().getResources().getDimension(R.dimen._2sdp));

            childLayout.setLayoutParams(linearParams);
            // create a new textview
            rowTextView = new TextView(App.getContext());
            rowTextViewOut = new TextView(App.getContext());
            rowTextView.setLayoutParams(new TableLayout.LayoutParams(
                    (int) App.getContext().getResources().getDimension(R.dimen._40sdp),
                    (int) App.getContext().getResources().getDimension(R.dimen._40sdp), 1f));
            rowTextViewOut.setLayoutParams(new TableLayout.LayoutParams(
                    (int) App.getContext().getResources().getDimension(R.dimen._40sdp),
                    (int) App.getContext().getResources().getDimension(R.dimen._40sdp), 1f));
            rowTextView.setGravity(Gravity.CENTER);
            rowTextViewOut.setGravity(Gravity.CENTER);
            childLayout.addView(rowTextViewOut, 0);
            childLayout.addView(rowTextView, 0);
            linearLayout.addView(childLayout);
            rowTextView.setTextSize((float) textAnimationFields.getTextSize());
            rowTextViewOut.setTextSize((float) textAnimationFields.getTextSize());
            rowTextViewOut.setTextColor(Color.BLACK);
            rowTextView.setTypeface(Typeface.defaultFromStyle(Typeface.BOLD));
            rowTextViewOut.setTypeface(Typeface.defaultFromStyle(Typeface.BOLD));
            rowTextView.setEnabled(false);
            rowTextView.setTextColor(Color.BLACK);
            rowTextViewOut.setEnabled(false);
            rowTextView.setInputType(InputType.TYPE_CLASS_NUMBER);
            rowTextViewOut.setInputType(InputType.TYPE_CLASS_NUMBER);
            //rowTextView.setFilters(new InputFilter[] { new InputFilter.LengthFilter(1) });
            //rowTextViewOut.setFilters(new InputFilter[] { new InputFilter.LengthFilter(10) });
            myTextViews[i] = rowTextView;
            myTextViewsOut[i] = rowTextViewOut;
            animateTexts(animatingNumbers[i], 0, myTextViews[i], myTextViewsOut[i], textAnimationFields);
        }
    }

    public static void animateTexts(final int actualNo, final int loopNo,
                             final TextView textView, final TextView textViewOut,
                             TextAnimationFields textAnimationFields) {
        textViewOut.setText(" " + loopNo + " ");
        textView.setVisibility(View.GONE);
        if (actualNo == loopNo) {
            textViewOut.setText(" " + actualNo + " ");
            textView.setText(" " + actualNo + " ");
            textView.setVisibility(View.VISIBLE);
        } else {
            AnimatorSet animatorSet2 = new AnimatorSet();
            animatorSet2.setInterpolator(new LinearInterpolator());
            animatorSet2.playTogether(
                    ObjectAnimator.ofFloat(textViewOut, "translationY", 0, (textAnimationFields.getTextSize() * 3))
            );
            animatorSet2.setDuration(textAnimationFields.getAnimationDuration());
            animatorSet2.addListener(new AnimatorListenerAdapter() {

                @Override
                public void onAnimationEnd(Animator animation) {
                    super.onAnimationEnd(animation);
                }
            });
            animatorSet2.start();
            Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    textView.setVisibility(View.VISIBLE);
                    AnimatorSet animatorSet2 = new AnimatorSet();
                    animatorSet2.playTogether(
                            ObjectAnimator.ofFloat(textView, "translationY", -(textAnimationFields.getTextSize() * 3), 0)
                    );
                    animatorSet2.setDuration(textAnimationFields.getAnimationDuration());
                    animatorSet2.setInterpolator(new LinearInterpolator());
                    animatorSet2.addListener(new AnimatorListenerAdapter() {
                        @Override
                        public void onAnimationStart(Animator animation) {
                            super.onAnimationStart(animation);
                            if (actualNo < loopNo)
                                textView.setText(" " + (loopNo - 1) + " ");
                            else
                                textView.setText(" " + (loopNo + 1) + " ");
                        }

                        @Override
                        public void onAnimationEnd(Animator animation) {
                            super.onAnimationEnd(animation);
                            if (actualNo < loopNo)
                                animateTexts(actualNo, loopNo - 1, textView, textViewOut, textAnimationFields);
                            else
                                animateTexts(actualNo, loopNo + 1, textView, textViewOut, textAnimationFields);
                        }
                    });
                    animatorSet2.start();
                }
            }, textAnimationFields.getGapBetweenTwoNumbersDuration());
        }
    }

    public static int convertStringToInt(String val){
        try {
            return Integer.parseInt(val);
        }
        catch (NumberFormatException ex){
            return 0;
        }
    }

    public static int getRandomVal() {
        Random ran = new Random();
        return ran.nextInt(7);
    }

    public static void setCustomToastMessage(Activity activity, String msg, int resId) {
        LayoutInflater inflater = activity.getLayoutInflater();
        View alertView = inflater.inflate(resId, null);
        TextView toastMsg = alertView.findViewById(R.id.toastMsg);
        toastMsg.setText(msg);
        /*Toast toast = new Toast(activity);
        toast.setGravity(Gravity.FILL_HORIZONTAL, 100, 1);
        toast.setDuration(Toast.LENGTH_SHORT);
        toast.setView(alertView);
        toast.show();*/
        MaterialAlertDialogBuilder builder = new MaterialAlertDialogBuilder(activity)
                .setView(alertView);
        androidx.appcompat.app.AlertDialog alert = builder.create();
        alert.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        alert.setOwnerActivity(activity);
        alert.setCancelable(false);
        alert.setCanceledOnTouchOutside(false);
        alert.getWindow().getAttributes().windowAnimations = R.style.dialog_animation;
        alert.show();
    }

    public static Animation setRotateIndicator() {
        Animation anim = new AlphaAnimation(0.0f, 1.0f);
        anim.setDuration(500); //You can manage the blinking time with this parameter
        anim.setStartOffset(20);
        anim.setRepeatMode(Animation.REVERSE);
        anim.setRepeatCount(3);
        return anim;
    }
}
