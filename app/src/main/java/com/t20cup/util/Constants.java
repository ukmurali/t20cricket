package com.t20cup.util;

public class Constants {
    public static final String TARGET = "195";
    public static final String PLAYER_1 = "Kirat Bolhi";
    public static final String PLAYER_2 = "NS Nodhi";
    public static final String PLAYER_3 = "R Rumrah";
    public static final String PLAYER_4 = "Shashi Henra";
    public static final String PLAYER_1_SCORE = "5";
    public static final String PLAYER_2_SCORE = "3";
    public static final String PLAYER_SCORE = "0";
    public static final String BALL_DEFAULT = "0";
    public static final String WKT_DEFAULT = "7";
    public static final int BALL_DEFAULT_COUNT = 1;
    public static final int OVER_DEFAULT_COUNT = 16;
    public static final int SCORE_DEFAULT_COUNT = 155;
    public static final String ANIM_BASE_URL = "https://assets2.lottiefiles.com/packages/";
    public static final String SIX = ANIM_BASE_URL+"lf20_XCWz7k.json";
    public static final String OUT = ANIM_BASE_URL+"lf20_GJzRLr.json";
    public static final String FOUR = ANIM_BASE_URL+"lf20_Ff1E2w.json";
}
